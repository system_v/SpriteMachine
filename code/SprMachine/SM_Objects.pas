unit SM_Objects;

{$mode ObjFPC}{$H+}
{$ModeSwitch advancedrecords}
{$WARN 6058 off : Call to subroutine "$1" marked as inline is not inlined}
interface

uses
  Classes, SysUtils, fgl, CastleTransform, CastleScene, FPLIL, IniFiles;

type
  PStateEntry = ^TStateEntry;
  TStateEntry = record
    Id        : AnsiString;
    AnimSet   : AnsiString;
    Frame     : Integer;
    Time      : Integer;
    ActionCode: String;

    LuaCode : Boolean;
    AngledSprites : Boolean;
    GotoLabel : AnsiString;
    GotoOffset: Integer;

    NextState : PStateEntry;
  end;

  TStateGroupInfo = record
    Angled : Boolean;
    Mirrored : Boolean;
  end;
  PStateGroupInfo = ^TStateGroupInfo;

  TStateEntries = specialize TFPGList<PStateEntry>;
  TStateGroups  = specialize TFPGMapObject<String, TStateEntries>;
  TStateGroupsInfo = specialize TFPGMap<String, PStateGroupInfo>;

  { TActorClassDefinition }

  TActorClassDefinition = class(TObject)
  private
    function GetSpritesheetURI : String;
  protected
    FStatesGroups : TStateGroups;
    FStateGrpInfo : TStateGroupsInfo;
    FActorProperties: TMemIniFile;
    FSpriteSheetURI : String;
  public
    ActorClassName : AnsiString;

    ActorType      : AnsiString;

    constructor Create;
    destructor Destroy; override;

    procedure DumpStatesToLog;

    property SpriteSheetURI : String read GetSpritesheetURI write FSpriteSheetURI;
    property States : TStateGroups read FStatesGroups;
    property GroupInfo : TStateGroupsInfo read FStateGrpInfo;
    property ActorProps : TMemIniFile read FActorProperties write FActorProperties;

  end;
  TActorClasses = specialize TFPGObjectList<TActorClassDefinition>;

  { TSpriteMachineBehavior }

  TSpriteMachineBehavior = class(TCastleBehavior)
  private
    FActorDef : TActorClassDefinition;
    FCScene   : TCastleScene;
    FState    : PStateEntry;
    FStateGrpInfo : PStateGroupInfo;
    FStateLabel   : String;
    FLock     : Boolean;
    FFCount   : Integer;
    FAngleChoice  : Integer;
    FOldAngleChoice : Integer;
    FPlayAnim     : String;
  protected
    procedure ParentAfterAttach; override;
    procedure ProcessAnimation(pS : PStateEntry);
    procedure SetAngleChoice(Value : Integer);
  public
    constructor Create(AOwner: TComponent; ActorDef : TActorClassDefinition);
    procedure Update(const SecondsPassed: Single; var RemoveMe: TRemoveType); override;
    procedure SwitchState(StateLabel : String);

    property ActorDefinition : TActorClassDefinition read FActorDef;
    property State : PStateEntry read FState;
    property AngleChoice : Integer read FAngleChoice write SetAngleChoice;
    property CPlayAnim : String read FPlayAnim;
  end;

var
  ActorClassRegistry : TActorClasses;


implementation

uses CastleLog, LILBaseActorFunctions, CastleVectors, SM_Parser;

{ TSpriteMachineBehavior }

procedure TSpriteMachineBehavior.ParentAfterAttach;
begin
  inherited ParentAfterAttach;
  FCScene := Parent as TCastleScene;
end;

procedure TSpriteMachineBehavior.ProcessAnimation(pS: PStateEntry);
var
  GrpInfo : TStateGroupInfo;
  PlayAnim : String;
begin

  GrpInfo:= FStateGrpInfo^;

  if GrpInfo.Angled then begin

     if GrpInfo.Mirrored then begin

        case FAngleChoice of
          0, 4:  PlayAnim:= Format('angle%d/%s', [FAngleChoice, pS^.AnimSet]);
          1, 5:  PlayAnim:= Format('angle1/%s', [pS^.AnimSet]);
          2, 6:  PlayAnim:= Format('angle2/%s', [pS^.AnimSet]);
          3, 7:  PlayAnim:= Format('angle3/%s', [pS^.AnimSet]);
        end;

     end else PlayAnim:= Format('angle%d/%s', [FAngleChoice, pS^.AnimSet]);
     FPlayAnim:= PlayAnim;
     FCScene.ForceAnimationPose(PlayAnim, pS^.Frame-1, False);
  end else
     FCScene.ForceAnimationPose(FState^.AnimSet, FState^.Frame-1, False);

  if FState^.ActionCode <> '' then
     SpriteMachineVM.Execute(FState^.ActionCode, Self);
end;

procedure TSpriteMachineBehavior.SetAngleChoice(Value: Integer);
begin
  if Value = FAngleChoice then Exit;
  FAngleChoice:= Value;
  case FAngleChoice of
     1..3: FCScene.Scale := Vector3(-FCScene.Scale.X, FCScene.Scale.Y, -FCScene.Scale.Z);
     else  FCScene.Scale := FCScene.Scale.Abs;
   end;
end;

constructor TSpriteMachineBehavior.Create(AOwner: TComponent;
  ActorDef: TActorClassDefinition);
begin
  inherited Create(AOwner);
  FActorDef := ActorDef;
  FFCount:= 0;
  FAngleChoice:= 0;
  FLock:= false;
end;

procedure TSpriteMachineBehavior.Update(const SecondsPassed: Single;
  var RemoveMe: TRemoveType);
var
  NextStateGroup : Integer;
begin
  inherited Update(SecondsPassed, RemoveMe);
  Inc(FFCount);
  if FState = nil then Exit;

  {$push}
  {$BoolEval off}
  if (FState <> nil) and ((FFCount mod FState^.Time) <> 0) then
  {$pop}
  begin
     if FLock then Exit;
     FLock:= True;

     ProcessAnimation(FState);
     //if FActorDef.ActorClassName = 'trooper' then
     //   WritelnLog('Rollin Frame: %d - State Id: %s', [FFCount, FState^.Id]);
  end else
  begin

    if FState^.NextState = nil then
    begin
       //We have an Goto?
       if FState^.GotoLabel <> 'null' then begin
          FState:= FActorDef.States[FState^.GotoLabel][FState^.GotoOffset];
          FLock:= False;
          Exit;
       end;
       //If not go to the next state group.
       NextStateGroup := FActorDef.States.IndexOf(FStateLabel) + 1;
       if NextStateGroup < FActorDef.States.Count-1 then
       begin
          SwitchState(FActorDef.States.Keys[NextStateGroup]);
          Exit;
       end;

       //No gotos, no next state group?
       //Just get stuck in the frame.
       FState^.NextState := FState;
    end;
    FState:= FState^.NextState;
    FLock:= False;
    //WritelnLog('Switching state!');
  end;
end;

procedure TSpriteMachineBehavior.SwitchState(StateLabel: String);
begin

  if FActorDef.States.IndexOf(StateLabel) = -1 then
  begin
     raise Exception.CreateFmt('TSpriteMachineBehavior error!: %s state label not found!', [StateLabel]);
     FState:= nil;
     Exit;
  end;

  FState := FActorDef.States[StateLabel].First;
  FStateLabel := StateLabel;
  FStateGrpInfo := FActorDef.GroupInfo[StateLabel];
  FLock := False;
  WritelnLog('Switching state: %s ', [FState^.Id]);
end;

{ TActorClassDefinition }
(*
procedure TActorClassDefinition.AddState(S: PStateEntry);
begin


  if State = nil then
     State:= S
  else begin
    Prev := State;
    while Prev^.Next <> nil do
       Prev:= Prev^.Next;
    Prev^.Next:= State;
  end;
  State^.Next:= nil;
end;
*)

procedure TActorClassDefinition.DumpStatesToLog;
function PrintNextState(NS: PStateEntry) : String;
begin
  if NS = nil then
  begin
     Result := '<NULL>';
     Exit;
  end;

  Result := Format('State Id:%s, AnimSet:%s, Frame:%d, Time:%d, Next is NULL?:%s',
      [NS^.Id, NS^.AnimSet, NS^.Frame, NS^.Time, BoolToStr((NS^.NextState=nil), True)]);
end;

var
  i,j : Integer;
  E : PStateEntry;
  sections, props: TStringList;
begin
  //Trying linear.
  WritelnLog('======= Actor: %s LINEAR METHOD ==========', [Self.ActorClassName]);
  for i := 0 to FStatesGroups.Count-1 do
  begin
    WritelnLog('StateGroup: %s',[FStatesGroups.Keys[i]]);
    for E in FStatesGroups.Data[i] do
      WritelnLog('State Id:%s, AnimSet:%s, Frame:%d, Time:%d, Next:[%s]',
      [E^.Id, E^.AnimSet, E^.Frame, E^.Time, PrintNextState(E^.NextState)]);
  end;

  if not Assigned(FActorProperties) then Exit;
  WritelnLog('======= Actor: %s PROPERTIES ==========', [Self.ActorClassName]);

  sections := TStringList.Create;
  props := TStringList.Create;
  FActorProperties.ReadSections(sections);
  for i := 0 to sections.Count-1 do
  begin
    props.Clear;
    FActorProperties.ReadSection(sections[i], props);
    WritelnLog('[Section: %s]', [sections[i]]);
    for j := 0 to props.Count-1 do
        WritelnLog('%s=%s',[props[j], FActorProperties.ReadString(sections[i], props[j],'undefined')]);
  end;


  (*DO NOT USE IT IF THE SCRIPT HAS LOOP/WAIT Functions. *)
  (*
  WriteLnLog('===== LINKED LIST METHOD ============');
  E := FStatesGroups.Data[0].First;
  repeat
    WritelnLog('State Id:%s, AnimSet:%s, Frame:%d, Time:%g, Next is NULL?:%s',
      [E^.Id, E^.AnimSet, E^.Frame, E^.Time, BoolToStr((E^.NextState=nil), True)]);
    E:= E^.NextState;
  until E = nil;   *)
end;

function TActorClassDefinition.GetSpritesheetURI: String;
begin
  Result := FSpriteSheetURI;
  if Pos('.starling-xml', FSpriteSheetURI, 0) <> 0 then
     Result := Format('%s#fps:1', [FSpriteSheetURI]);
end;

constructor TActorClassDefinition.Create;
begin
  FStatesGroups := TStateGroups.Create(False);
  FStateGrpInfo := TStateGroupsInfo.Create;
end;

destructor TActorClassDefinition.Destroy;
begin
  inherited Destroy;
end;


end.

