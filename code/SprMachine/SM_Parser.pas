unit SM_Parser;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, FPLIL, X3DNodes, X3DFields, X3DTime, CastleScene;

type

  { TSpriteMachineNodeBuilder }

  TSpriteMachineNodeBuilder = class
  private
    FRoot : TX3DRootNode;
    FTimeSensor : TTimeSensorNode;
    FSpriteNode : TImageTextureNode;
    FSpriteSequencer : TIntegerSequencerNode;
    FSpriteList : TStringList;
    FSprIndex   : Integer;
    procedure ListSpriteFiles;
    procedure OnFracChanged(const Event: TX3DEvent; const Value: TX3DField;
    const Time: TX3DTime);
  public
    constructor Create;
    procedure CreateTestNode;
    procedure UseDefaultSpeed;

    property TimeSensor : TTimeSensorNode read FTimeSensor;
    property RootNode : TX3DRootNode read FRoot;
  end;

  { TSpriteMachineParser }

  TSpriteMachineParser = class(TObject)
  private
    FLilVM : TLIL;
    FScriptURI : String;
    procedure OnScriptError(LIL: TLIL; APosition: Integer; AMessage: string);
    procedure OnSourceInclude(LIL: TLIL; AFileName: string; out AContent: string);
    procedure OnFracChangedTimeNode(const Event: TX3DEvent; const Value: TX3DField;
    const Time: TX3DTime);
  public
    constructor Create;
    destructor Destroy; override;

    procedure Parse(const URI : String);
    procedure Execute(code : String; ActorObject : Pointer = nil);
    property LilVM : TLIL read FLilVM;
  end;


var
  SpriteMachineVM : TSpriteMachineParser;

implementation

uses SM_Objects,CastleFilesUtils, CastleDownload, CastleVectors,CastleColors,
  LILBaseActorFunctions, SMLIL_UtilFunctions, CastleFindFiles, CastleURIUtils,
  CastleLog, Math;

{ TSpriteMachineNodeBuilder }

procedure TSpriteMachineNodeBuilder.ListSpriteFiles;
var
  L: TFileInfoList;
  I: TFileInfo;
  IsRelative : Boolean;
  FileURI : String;
begin
  FSpriteList.Clear;
  L := FindFilesList('castle-data:/SPRITES', '*.png', False, []);
  for I in L do
  begin
    FileURI := RelativeToCastleDataURL(I.AbsoluteName, IsRelative);
    WritelnLog('Listing Sprite: %s', [FileURI]);

    if IsRelative then
       FSpriteList.Add(Format('castle-data:/%s', [FileURI]));
  end;
end;

procedure TSpriteMachineNodeBuilder.OnFracChanged(const Event: TX3DEvent;
  const Value: TX3DField; const Time: TX3DTime);
begin
  FSpriteNode.SetUrl([FSpriteList[Min( (Value as TSFInt32).Value, FSpriteList.Count-1)]]);
  //Inc(FSprIndex);
  WriteLnLog('SpriteIndex: %d', [(Value as TSFInt32).Value]);
end;

constructor TSpriteMachineNodeBuilder.Create;
begin
  FSpriteList := TStringList.Create;
  ListSpriteFiles;
  FSprIndex:= 0;
end;

procedure TSpriteMachineNodeBuilder.CreateTestNode;
var
  Shape : TShapeNode;
  Material : TUnlitMaterialNode;
  AppNode  : TAppearanceNode;
  Plane : TPlaneNode;
  Transform : TTransformNode;
  I : Integer;
  Key, Total : Single;
const
  SampleDurations : array[0..9] of Single = (3, 0, 7, 7, 7, 7, 7, 6, 6, 5);
begin
  if not Assigned(FRoot) then FRoot := TX3DRootNode.Create('TestX3DNode') else FRoot.ClearChildren;
  Material := TUnlitMaterialNode.Create;
  AppNode := TAppearanceNode.Create;
  AppNode.Material := Material;
  Plane := TPlaneNode.CreateWithTransform(Shape, Transform);
  Shape.Appearance := AppNode;
  FSpriteNode := TImageTextureNode.Create;
  FSpriteNode.SetUrl([FSpriteList[0]]);
  Shape.Appearance.Texture := FSpriteNode;

  FRoot.AddChildren(Transform);

  FTimeSensor := TTimeSensorNode.Create('TestAnim');
  FTimeSensor.CycleInterval:= (9/35);
  FSpriteSequencer := TIntegerSequencerNode.Create;

  Key := 0;
  for I := 0 to 9 do
  begin
    Key := (SampleDurations[I]) * (I/35);
    WritelnLog('Key Time #%d: %g', [I, Key]);
    FSpriteSequencer.FdKey.Items.Add(Key);
    FSpriteSequencer.FdKeyValue.Items.Add(I);
  end;


  (*
  Total := 0;
  for I := 0 to 9 do
  begin
    Key:= (I / 9);
    Total := Total + Key;
    WritelnLog('Key Time #%d: %g', [I, Key]);
    FSpriteSequencer.FdKey.Items.Add(Key);
    FSpriteSequencer.FdKeyValue.Items.Add(I);
  end;
  WritelnLog('Total Key Duration: %g', [Total]);
  *)

  FRoot.AddRoute(FTimeSensor.EventFraction_changed, FSpriteSequencer.EventSet_fraction);
  FSpriteSequencer.EventValue_changed.AddNotification(@OnFracChanged);
  FRoot.AddChildren(FTimeSensor);

end;

procedure TSpriteMachineNodeBuilder.UseDefaultSpeed;
begin
  FTimeSensor.CycleInterval:= (FSpriteList.Count-1);
end;

{ TSpriteMachineParser }

procedure TSpriteMachineParser.OnScriptError(LIL: TLIL; APosition: Integer;
  AMessage: string);
begin
  WritelnLog('LIL SCRIPT ERROR at %d: %s', [APosition, AMessage]);
end;

procedure TSpriteMachineParser.OnSourceInclude(LIL: TLIL; AFileName: string;
  out AContent: string);
var
  SourceScript : TStringList;
begin
  SourceScript := TStringList.Create;
  SourceScript.LoadFromURL(Format('castle-data:/%s', [AFileName]));
  AContent:= SourceScript.Text;
end;

procedure TSpriteMachineParser.OnFracChangedTimeNode(const Event: TX3DEvent;
  const Value: TX3DField; const Time: TX3DTime);
begin
  WritelnLog('FractionChanged Event Tick: %g!', [Time.Seconds]);
end;

constructor TSpriteMachineParser.Create;
begin

  FLilVM := TLIL.Create(nil);
  FLilVM.OnError:=@OnScriptError;
  FLilVM.OnSource:=@OnSourceInclude;
  RegisterBaseActorLILFunctions(FLilVM);
  RegisterLILUtilityFunctions(FLilVM);

end;

destructor TSpriteMachineParser.Destroy;
begin
  FLilVM.Free;
  inherited Destroy;
end;

procedure TSpriteMachineParser.Parse(const URI : String);
var
  Script : TStringList;
begin
  FScriptURI:= URI;
  Script := TStringList.Create;
  Script.LoadFromURL(FScriptURI);
  FLilVM.Parse(Script.Text);
  Script.Clear;
  Script.Free;
end;

procedure TSpriteMachineParser.Execute(code: String; ActorObject: Pointer);
begin
  FLilVM.Data:= ActorObject;
  FLilVM.Parse(code);
end;

initialization
  SpriteMachineVM := TSpriteMachineParser.Create;
finalization
  SpriteMachineVM.Free;

end.

