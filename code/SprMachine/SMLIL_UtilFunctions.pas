unit SMLIL_UtilFunctions;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, FPLIL;

procedure RegisterLILUtilityFunctions(VM : TLIL);

function Util_Random(LIL : TLIL; Args : TLILFunctionProcArgs) : TLILValue;
function Util_AChance(LIL : TLIL; Args : TLILFunctionProcArgs) : TLILValue;
function Util_RandomRange(LIL : TLIL; Args : TLILFunctionProcArgs) : TLILValue;
function Util_AJump(LIL : TLIL; Args : TLILFunctionProcArgs) : TLILValue;
function Util_APlaySound(LIL : TLIL; Args : TLILFunctionProcArgs) : TLILValue;

implementation

uses
  Math, CastleLog, CastleSoundEngine, SM_Objects;

var
  Snd : TCastleSound;

procedure RegisterLILUtilityFunctions(VM: TLIL);
begin
  VM.Register('RandomInt', @Util_Random);
  VM.Register('RandomRangeInt', @Util_RandomRange);
  VM.Register('AJump', @Util_AJump);
  VM.Register('AChance', @Util_AChance);
  VM.Register('APlaySound', @Util_APlaySound);
end;

function Util_Random(LIL: TLIL; Args: TLILFunctionProcArgs): TLILValue;
begin
  Result := LIL.AllocInteger(Round(Random));
end;

function Util_AChance(LIL: TLIL; Args: TLILFunctionProcArgs): TLILValue;
begin
  Result := LIL.AllocBoolean( (Random(100)) < Args[0].IntegerValue );
end;

function Util_RandomRange(LIL: TLIL; Args: TLILFunctionProcArgs): TLILValue;
begin
  Result := LIL.AllocInteger(RandomRange(Args[0].IntegerValue, Args[1].IntegerValue));
end;

function Util_AJump(LIL: TLIL; Args: TLILFunctionProcArgs): TLILValue;
var
  Chance : Boolean;
  JumpList: TLILList;
  SelIndex: Integer;
  StateJump : String;
  SMBehav   : TSpriteMachineBehavior;
begin
  Result := nil;
  Chance := Random(100) < Args[0].IntegerValue;
  if Chance then
  begin
    JumpList := LIL.SubstituteToList(Args[1]);
    SelIndex := Random(JumpList.Count);
    StateJump := JumpList.Values[SelIndex].StringValue;
    SMBehav := TSpriteMachineBehavior(LIL.Data);
    SMBehav.SwitchState(StateJump);
    FreeAndNil(JumpList);
  end;
end;

function Util_APlaySound(LIL: TLIL; Args: TLILFunctionProcArgs): TLILValue;
var
  SndURL : String;
begin
  if not Assigned(Snd) then Snd := TCastleSound.Create(nil);
  SndURL:= Format('castle-data:/%s',[Args[0].StringValue]);
  Snd.URL:= SndURL;
  SoundEngine.Play(Snd);
  Result := nil;
end;

end.

