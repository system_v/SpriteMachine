unit LILBaseActorFunctions;

{$mode ObjFPC}{$H+}
{$WARN 6058 off : Call to subroutine "$1" marked as inline is not inlined}
interface

uses
  Classes, SysUtils, FPLIL;


procedure RegisterBaseActorLILFunctions(LILVM : TLIL);

function ActorNewDefinition(LIL : TLIL; Args : TLILFunctionProcArgs) : TLILValue;
function ActorProperties(LIL : TLIL; Args : TLILFunctionProcArgs) : TLILValue;
function StatesActorDefinition(LIL : TLIL; Args : TLILFunctionProcArgs) : TLILValue;
function StateDefinition(LIL : TLIL; Args : TLILFunctionProcArgs) : TLILValue;
function FrameDefinition(LIL : TLIL; Args : TLILFunctionProcArgs) : TLILValue;
function MulFrameDefinition(LIL : TLIL; Args : TLILFunctionProcArgs) : TLILValue;

function FrameSpecial_Loop(LIL : TLIL; Args : TLILFunctionProcArgs) : TLILValue;
function FrameSpecial_Goto(LIL : TLIL; Args : TLILFunctionProcArgs) : TLILValue;    
function FrameSpecial_Wait(LIL : TLIL; Args : TLILFunctionProcArgs) : TLILValue;
function FrameSpecial_Stop(LIL : TLIL; Args : TLILFunctionProcArgs) : TLILValue;

function APlaySound(LIL : TLIL; Args : TLILFunctionProcArgs) : TLILValue;


implementation

uses
  SM_Objects,CastleLog, StrUtils, Types, CastleSoundEngine, IniFiles;

var
  PrevState, LoopStart, LastState : PStateEntry;
  StateIdx : Integer;
  StateLabel : String;
  APlaySnd : TCastleSound;

function PreprocessActionCode(s : String; pS : PStateEntry) : String;
begin
  Result := s;
  pS^.LuaCode:= False;
  WritelnLog('[PreprocessActionCode] code: %s', [s]);
  if s = '' then Exit;

  if Pos('%lua%', s, 0) <> 0 then begin
    Delete(Result, 1, Pos('%lua%', s, 0));
    pS^.LuaCode:= True;
  end;
end;

procedure RegisterBaseActorLILFunctions(LILVM: TLIL);
begin
  if not Assigned(LILVM) then Exit;

  LILVM.Register('Actor', @ActorNewDefinition);
  LILVM.Register('Properties', @ActorProperties);
  LILVM.Register('States', @StatesActorDefinition);
  LILVM.Register('State',  @StateDefinition);
  LILVM.Register('Sprite',  @FrameDefinition);
  LILVM.Register('Sprites', @MulFrameDefinition);
  LILVM.Register('Loop', @FrameSpecial_Loop);
  LILVM.Register('Goto', @FrameSpecial_Goto);
  LILVM.Register('Wait', @FrameSpecial_Wait);
  LILVM.Register('Stop', @FrameSpecial_Stop);
end;

function ActorNewDefinition(LIL : TLIL; Args : TLILFunctionProcArgs) : TLILValue;
var
  NClass : TActorClassDefinition;
begin
  StateIdx:= 0;
  PrevState:= nil;
  LoopStart:= nil;
  LastState:= nil;
  NClass := TActorClassDefinition.Create;
  NClass.ActorClassName:= Args[0].StringValue;
  NClass.ActorType:= Args[1].StringValue;
  NClass.SpriteSheetURI:= Format('castle-data:/%s', [Args[2].StringValue]);

  LIL.Data:= NClass;
  LIL.Parse(Args[3].StringValue, True);
  ActorClassRegistry.Add(NClass);
  Result := nil;
end;

function ActorProperties(LIL : TLIL; Args : TLILFunctionProcArgs) : TLILValue;
var
  NC : TActorClassDefinition;
  I : Integer;
  StrStream : TStringStream;
begin
  NC := TActorClassDefinition(LIL.Data);
  StrStream := TStringStream.Create(Args[0].StringValue);
  NC.ActorProps := TMemIniFile.Create(StrStream);  //Ez way.
  Result := nil;
end;

function StatesActorDefinition(LIL : TLIL; Args : TLILFunctionProcArgs) : TLILValue;
var
  NC : TActorClassDefinition;
begin
  NC := TActorClassDefinition(LIL.Data);
  LIL.Parse(Args[0].StringValue, True);
  Result := nil;
end;

function StateDefinition(LIL : TLIL; Args : TLILFunctionProcArgs) : TLILValue;
var
  NC : TActorClassDefinition;
  NewStateGroup : TStateEntries;
  NewStateGroupInfo  : PStateGroupInfo;
  i, N : Integer;
begin
  NC := TActorClassDefinition(LIL.Data);
  NewStateGroup := TStateEntries.Create;
  New(NewStateGroupInfo);
  NewStateGroupInfo^.Angled:=False;
  NewStateGroupInfo^.Mirrored:= False;
  StateIdx:=0;
  StateLabel:= Args[0].StringValue;
  N := 1;

  for i := 1 to Length(Args)-1 do
  begin
    if LowerCase(Args[i].StringValue) = 'mirrored' then
    begin
       NewStateGroupInfo^.Mirrored:= True;
       Inc(N);
    end;

    if LowerCase(Args[i].StringValue) = 'angled' then
    begin
       NewStateGroupInfo^.Angled:= True;
       Inc(N);
    end;
  end;

  NC.States.Add(StateLabel, NewStateGroup);
  NC.GroupInfo.Add(StateLabel, NewStateGroupInfo);

  LIL.Parse(Args[N].StringValue, True);

  Result := nil;
end;

function FrameDefinition(LIL : TLIL; Args : TLILFunctionProcArgs) : TLILValue;
var
  InClass : TActorClassDefinition;
  CurrentState : PStateEntry;
begin
  InClass := TActorClassDefinition(LIL.Data);

  New(CurrentState);
  with CurrentState^ do begin
    Id:= Format('S_%s_%d', [StateLabel, StateIdx]);
    AnimSet:= Args[0].StringValue;
    Frame:= Args[1].IntegerValue;
    Time := Args[2].IntegerValue;
    AngledSprites:= False;
    GotoLabel:= 'null';
    GotoOffset:= Integer.MaxValue;
    NextState:= nil; //just in case.
  end;

  if Length(Args) > 3 then
     CurrentState^.ActionCode:= PreprocessActionCode(Args[3].StringValue, CurrentState);


  if PrevState <> nil then
     PrevState^.NextState:= CurrentState;

  LoopStart:= CurrentState;
  PrevState:= CurrentState;
  LastState:= CurrentState;

  InClass.States.KeyData[StateLabel].Add(CurrentState);
  Inc(StateIdx);
  Result := nil;
end;

function MulFrameDefinition(LIL: TLIL; Args: TLILFunctionProcArgs): TLILValue;
var
  InClass : TActorClassDefinition;
  NewState : PStateEntry;
  i : Integer;
  FrameList : TLILList;
begin
  Result := nil;

  InClass := TActorClassDefinition(LIL.Data);
  FrameList := LIL.SubstituteToList(Args[1]);
  if FrameList.Count < 1 then
  begin
     raise Exception.Create('Frame separators (,) are invalid or not present!');
     Exit;
  end;

  for i := 0 to FrameList.Count-1 do
  begin
    New(NewState);
    with NewState^ do begin
      Id:= Format('S_%s_%d', [StateLabel, i]);
      WriteLnLog('New state: %s, frameIdx: %d', [Id, FrameList.Values[i].IntegerValue]);
      AnimSet:= Args[0].StringValue;
      Frame:= FrameList.Values[i].IntegerValue;
      Time := Args[2].IntegerValue;
      if Length(Args) > 3 then ActionCode:=PreprocessActionCode(Args[3].StringValue, NewState);
      NextState:= nil;
      AngledSprites:= False;
    end;
    if PrevState <> nil then PrevState^.NextState:= NewState;
    PrevState:= NewState;
    LastState:= NewState;
    InClass.States[StateLabel].Add(NewState);
  end;
  FrameList.Free;

end;

function FrameSpecial_Loop(LIL : TLIL; Args : TLILFunctionProcArgs) : TLILValue;
var
  InClass : TActorClassDefinition;
begin
  Result := nil;

  if LastState = nil then
  begin
     raise Exception.Create('Loop before first state!');
     Exit;
  end;

  InClass := TActorClassDefinition(LIL.Data);

  LastState^.NextState:= InClass.States[StateLabel].First;
  WritelnLog('[Goto function]: StateGroup: %s Last-state id: %s, ', [StateLabel,LastState^.Id]);
  PrevState:= nil;
end;

function FrameSpecial_Goto(LIL : TLIL; Args : TLILFunctionProcArgs) : TLILValue;
var
  GotoOffset : Integer;
begin
  Result := nil;

  if LastState = nil then begin
     raise Exception.Create('Goto before first state!');
     Exit;
  end;

  if LowerCase(Args[0].StringValue) = 'null' then
  begin
     raise Exception.Create('Using goto label named "null" is not permitted in actor state definition');
     Exit;
  end;

  if Length(Args) > 1 then
     GotoOffset:= Args[1].IntegerValue
  else GotoOffset:= 0;

  if LastState <> nil then
  begin
     LastState^.GotoLabel:= Args[0].StringValue;
     LastState^.GotoOffset:= GotoOffset;
  end;
  PrevState:= nil;
end;

function FrameSpecial_Wait(LIL: TLIL; Args: TLILFunctionProcArgs): TLILValue;
begin
  Result := nil;
  if LastState = nil then
  begin
     raise Exception.Create('Wait/Fail before first state!');
     Exit;
  end;

  LastState^.NextState:= LastState;
  PrevState:= nil;
end;

function FrameSpecial_Stop(LIL: TLIL; Args: TLILFunctionProcArgs): TLILValue;
var
  InClass : TActorClassDefinition;
begin
  Result := nil;
  InClass := TActorClassDefinition(LIL.Data);
  if (LastState = nil) and (InClass.States[StateLabel].Count = 0) then
  begin
     Raise Exception.Create('Stop before first state!');
     Exit;
  end;

  if LastState <> nil then LastState^.NextState:= nil;
  PrevState:= nil;
end;

function APlaySound(LIL: TLIL; Args: TLILFunctionProcArgs): TLILValue;
begin
  //This thing is nasty but should work.
  if not Assigned(APlaySnd) then APlaySnd := TCastleSound.Create(nil);
  APlaySnd.URL:= Format('castle-data:/%s', [Args[0].StringValue]);
  SoundEngine.Play(APlaySnd);
  Result := nil;
end;


end.

