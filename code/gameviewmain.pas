{ Main view, where most of the application logic takes place.

  Feel free to use this code as a starting point for your own projects.
  This template code is in public domain, unlike most other CGE code which
  is covered by BSD or LGPL (see https://castle-engine.io/license). }
unit GameViewMain;

interface

uses Classes,
  CastleVectors, CastleComponentSerialize, SM_Objects,
  CastleUIControls, CastleControls, CastleKeysMouse,CastleScene,SM_Parser;

type
  { Main view, where most of the application logic takes place. }

  { TViewMain }

  TViewMain = class(TCastleView)
  published
    { Components designed using CGE editor.
      These fields will be automatically initialized at Start. }
    LabelFps: TCastleLabel;
    SimpleSpriteScene  : TCastleScene;
    AngledSpriteScene  : TCastleScene;
    SpriteAngleSlider  : TCastleIntegerSlider;
    CurrentAngleLabel  : TCastleLabel;
    InfoASLabel : TCastleLabel;
    LoopFramesChk : TCastleCheckbox;
    UseDefaultChk : TCastleCheckbox;
    CustomSpeedEdit : TCastleFloatEdit;
    FPSEdit         : TCastleFloatEdit;
  private
    AS_SMBehavior : TSpriteMachineBehavior;
    TestBuilder : TSpriteMachineNodeBuilder;
    procedure OnAngleSliderChange(Sender : TObject);
    procedure OnCustomSpeedEditChange(Sender : TObject);
    procedure OnFPSEditChange(Sender : TObject);
    procedure OnLoopFramesCheck(Sender : TObject);
    procedure OnUseDefaultCheck(Sender : TObject);

  public
    constructor Create(AOwner: TComponent); override;
    procedure Start; override;
    procedure Update(const SecondsPassed: Single; var HandleInput: Boolean); override;
    function Press(const Event: TInputPressRelease): Boolean; override;
  end;

var
  ViewMain: TViewMain;

implementation

uses SysUtils, CastleLog, X3DNodes, X3DLoad;

{ TViewMain ----------------------------------------------------------------- }

constructor TViewMain.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/gameviewmain.castle-user-interface';
end;

procedure TViewMain.OnAngleSliderChange(Sender : TObject);
begin
  AS_SMBehavior.AngleChoice:= SpriteAngleSlider.Value;
end;

procedure TViewMain.OnCustomSpeedEditChange(Sender: TObject);
begin
  if not UseDefaultChk.Checked then
     TestBuilder.TimeSensor.CycleInterval:= CustomSpeedEdit.Value;

end;

procedure TViewMain.OnFPSEditChange(Sender: TObject);
var
  NewCycle,CycleInterval : Single;

begin
  CycleInterval:= TestBuilder.TimeSensor.CycleInterval;
  NewCycle:= CycleInterval / FPSEdit.Value;

  if NewCycle = CycleInterval then Exit;

  TestBuilder.TimeSensor.CycleInterval:= NewCycle;
end;

procedure TViewMain.OnLoopFramesCheck(Sender: TObject);
begin
  SimpleSpriteScene.PlayAnimation('TestAnim', LoopFramesChk.Checked);
end;

procedure TViewMain.OnUseDefaultCheck(Sender: TObject);
begin
  CustomSpeedEdit.Enabled:= not UseDefaultChk.Checked;

  if UseDefaultChk.Checked then
  begin
    TestBuilder.UseDefaultSpeed;
    CustomSpeedEdit.Value:= TestBuilder.TimeSensor.CycleInterval;
  end;
end;

procedure TViewMain.Start;
var
  ActorDef : TActorClassDefinition;
  SMBehavior : TSpriteMachineBehavior;

begin
  inherited;
  SpriteAngleSlider.OnChange:= @OnAngleSliderChange;
  //Temporal:
  ActorClassRegistry := TActorClasses.Create(True);
  CustomSpeedEdit.OnChange:= @OnCustomSpeedEditChange;
  FPSEdit.OnChange:= @OnFPSEditChange;
  LoopFramesChk.OnChange:=@OnLoopFramesCheck;
  UseDefaultChk.OnChange:=@OnUseDefaultCheck;




  TestBuilder := TSpriteMachineNodeBuilder.Create;
  TestBuilder.CreateTestNode;
  SimpleSpriteScene.Load(TestBuilder.RootNode, False);
  SimpleSpriteScene.Scale := SimpleSpriteScene.Scale*100;

  SimpleSpriteScene.PlayAnimation('TestAnim', LoopFramesChk.Checked);
  CustomSpeedEdit.Value:= TestBuilder.TimeSensor.CycleInterval;

  (*
  SpriteMachineVM.Parse('castle-data:/scripts/supermp5.lil');
  SpriteMachineVM.Parse('castle-data:/scripts/Trooper.lil');

  ActorDef := ActorClassRegistry[0];
  ActorDef.DumpStatesToLog;
  SimpleSpriteScene.URL:= ActorDef.SpriteSheetURI;
  SMBehavior := TSpriteMachineBehavior.Create(SimpleSpriteScene, ActorDef);
  SimpleSpriteScene.AddBehavior(SMBehavior);
  SMBehavior.SwitchState('Idle');

  ActorDef := ActorClassRegistry[1];
  ActorDef.DumpStatesToLog;
  AngledSpriteScene.URL:= ActorDef.SpriteSheetURI;
  SMBehavior := TSpriteMachineBehavior.Create(AngledSpriteScene, ActorDef);
  AngledSpriteScene.AddBehavior(SMBehavior);
  SMBehavior.SwitchState('walk');
  AngledSpriteScene.Scale := AngledSpriteScene.Scale*6;
  AS_SMBehavior := SMBehavior; *)

  //SimpleSpriteScene.URL:= 'castle-data:/weapons/supermp5/supermp5.castle-sprite-sheet';
  //SaveNode(SimpleSpriteScene.RootNode, 'test.x3d');
end;

procedure TViewMain.Update(const SecondsPassed: Single; var HandleInput: Boolean);
begin
  inherited;
  { This virtual method is executed every frame (many times per second). }
  LabelFps.Caption := 'FPS: ' + Container.Fps.ToString;

  //CurrentAngleLabel.Caption:= AS_SMBehavior.CPlayAnim;
  InfoASLabel.Caption:= Format('Scale: %s', [AngledSpriteScene.Scale.ToString]);
end;

function TViewMain.Press(const Event: TInputPressRelease): Boolean;
begin
  Result := inherited;
  if Result then Exit; // allow the ancestor to handle keys
end;

end.
